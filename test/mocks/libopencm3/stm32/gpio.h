#ifndef SAT_FIRMWARE_LAUNCHER_MOCK_LIBOPENCM3_GPIO
#define SAT_FIRMWARE_LAUNCHER_MOCK_LIBOPENCM3_GPIO

#include <cstdint>

#include "fff.h"

#define GPIOA 0
#define GPIO_MODE_OUTPUT 0
#define GPIO_PUPD_NONE 0
#define GPIO5 5

void gpio_mode_setup(uint32_t gpioport, uint8_t mode, uint8_t pull_up_down, uint16_t gpios);

#endif // SAT_FIRMWARE_LAUNCHER_MOCK_LIBOPENCM3_GPIO
