#ifndef SAT_FIRMWARE_LAUNCHER_SQUARE_ROOT_MOCK_H
#define SAT_FIRMWARE_LAUNCHER_SQUARE_ROOT_MOCK_H

#include "calculator/square_root.h"
#include <gmock/gmock.h>

class Mock_square_root : public calc::Square_root {
public:
    MOCK_METHOD(double, compute, (double x));
};

#endif // SAT_FIRMWARE_LAUNCHER_SQUARE_ROOT_MOCK_H
