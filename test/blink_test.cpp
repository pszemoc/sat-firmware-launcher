#include <gtest/gtest.h>
#include <fff.h>

#include "mocks/libopencm3/stm32/gpio.h"

#include "blink/blink.h"

DEFINE_FFF_GLOBALS;
FAKE_VOID_FUNC(gpio_mode_setup, uint32_t, uint8_t, uint8_t, uint16_t);

using namespace ::testing;

TEST(BlinkSuite, BlinkInits) {
    Blink blink{};
    ASSERT_EQ(gpio_mode_setup_fake.call_count, 1);
}
