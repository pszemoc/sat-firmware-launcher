#ifndef SAT_FIRMWARE_LAUNCHER_ETL_PROFILE_H
#define SAT_FIRMWARE_LAUNCHER_ETL_PROFILE_H

// For more detailed info visit these sites:
// https://www.etlcpp.com/setup.html
// https://www.etlcpp.com/no_stl.html
// https://www.etlcpp.com/macros.html
// I believe there is more documentation available there if needed

#define ETL_LOG_ERRORS     // ETL_ASSERT calls the error handler then asserts
#define ETL_VERBOSE_ERRORS // Error messages and output in their long form.
#define ETL_CHECK_PUSH_POP // Pushes and pops are checked for bounds

#define ETL_TARGET_OS_FREERTOS
#define ETL_TARGET_DEVICE_ARM

#endif // SAT_FIRMWARE_LAUNCHER_ETL_PROFILE_H
