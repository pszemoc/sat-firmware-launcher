cmake_minimum_required(VERSION 3.16)

if(${CMAKE_HOST_SYSTEM_NAME} STREQUAL Windows)
    set(EXEC_FILE ".exe")
endif()

set(CMAKE_SYSTEM_NAME generic)
set(CMAKE_PROCESSOR_TYPE arm)
# Setup toolchain
set(TOOLCHAIN_DIR ${CMAKE_CURRENT_SOURCE_DIR}/gcc-arm-none-eabi/bin)
set(CMAKE_C_COMPILER ${TOOLCHAIN_DIR}/arm-none-eabi-gcc${EXEC_FILE})
set(CMAKE_CXX_COMPILER ${TOOLCHAIN_DIR}/arm-none-eabi-g++${EXEC_FILE})
set(CMAKE_ASM_COMPILER ${TOOLCHAIN_DIR}/arm-none-eabi-gcc${EXEC_FILE})
set(CMAKE_AR ${TOOLCHAIN_DIR}/arm-none-eabi-ar${EXEC_FILE})
set(CMAKE_OBJCOPY ${TOOLCHAIN_DIR}/arm-none-eabi-objcopy${EXEC_FILE})
set(CMAKE_OBJDUMP ${TOOLCHAIN_DIR}/arm-none-eabi-objdump${EXEC_FILE})
set(SIZE ${TOOLCHAIN_DIR}/arm-none-eabi-size${EXEC_FILE})
set(DEBUGGER ${TOOLCHAIN_DIR}/arm-none-eabi-gdb${EXEC_FILE})
set(FLASH st-flash)
