cmake_minimum_required(VERSION 3.16)

set(CMAKE_SYSTEM_NAME generic)
set(CMAKE_PROCESSOR_TYPE arm)
# Setup toolchain
set(CMAKE_C_COMPILER arm-none-eabi-gcc)
set(CMAKE_CXX_COMPILER arm-none-eabi-g++)
set(CMAKE_ASM_COMPILER arm-none-eabi-gcc)
set(CMAKE_AR arm-none-eabi-ar)
set(CMAKE_OBJCOPY arm-none-eabi-objcopy)
set(CMAKE_OBJDUMP arm-none-eabi-objdump)
set(SIZE arm-none-eabi-size)
set(DEBUGGER arm-none-eabi-gdb)
set(FLASH st-flash)
