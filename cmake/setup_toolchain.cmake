cmake_minimum_required(VERSION 3.16)

macro(fix_cmake_defaults)
    # Fix harmful CMake defaults
    # Fix pthreads linkage on MacOS
    IF(APPLE)
        set(CMAKE_THREAD_LIBS_INIT "-lpthread")
        set(CMAKE_HAVE_THREADS_LIBRARY 1)
        set(CMAKE_USE_WIN32_THREADS_INIT 0)
        set(CMAKE_USE_PTHREADS_INIT 1)
        set(THREADS_PREFER_PTHREAD_FLAG ON)
    ENDIF()
endmacro(fix_cmake_defaults)


macro(setup_toolchain LOCAL_TOOLCHAIN_ENABLED)
    # Set CMake toolchain test to compile the standard library
    set(CMAKE_TRY_COMPILE_TARGET_TYPE "STATIC_LIBRARY")

    if(${LOCAL_TOOLCHAIN_ENABLED} STREQUAL ON)
        if(EXISTS ${CMAKE_SOURCE_DIR}/gcc-arm-none-eabi)
            message(${BOLD_YELLOW} "The local toolchain directory already "
                    "exits, will try to use it." ${NO_COLOR})
        else()
            message(${BOLD_YELLOW} "Downloading the toolchain to the project's "
                    "directory" ${NO_COLOR})
            get_toolchain()
        endif()

        set(CMAKE_TOOLCHAIN_FILE
            ${CMAKE_SOURCE_DIR}/cmake/local_toolchain.cmake
        )
    else()
        message(${BOLD_YELLOW} "Using the global toolchain, the "
                "gcc-arm-none-eabi should be in the PATH" ${NO_COLOR})
        set(CMAKE_TOOLCHAIN_FILE
            ${CMAKE_SOURCE_DIR}/cmake/global_toolchain.cmake
        )
    endif()
endmacro(setup_toolchain)


function(get_toolchain)
    set_toolchain_download_urls()

    set(TOOLCHAIN_NAME gcc-arm-none-eabi)
    set(TOOLCHAIN_NAME_DOWNLOAD arm-download)
    set(TOOLCHIAN_VERSION -9-2019-q4)
    set(DOWNLOAD_PATH ${CMAKE_SOURCE_DIR}/${TOOLCHAIN_NAME_DOWNLOAD})

    if(${CMAKE_HOST_SYSTEM_NAME} STREQUAL Linux)
        file(DOWNLOAD ${LINUX_URL} ${DOWNLOAD_PATH}
            SHOW_PROGRESS
            EXPECTED_HASH MD5=${LINUX_MD5}
        )
    elseif(${CMAKE_HOST_SYSTEM_NAME} STREQUAL Darwin)
        file(DOWNLOAD ${MAC_URL} ${DOWNLOAD_PATH}
            SHOW_PROGRESS
            EXPECTED_HASH MD5=${MAC_MD5}
        )
    elseif(${CMAKE_HOST_SYSTEM_NAME} STREQUAL Windows)
        file(DOWNLOAD ${WIN_URL} ${DOWNLOAD_PATH}
            SHOW_PROGRESS
            EXPECTED_HASH MD5=${WIN_MD5}
        )
    else()
        message(WARNING "Unsupported system, will not install the toolchain")
        return()
    endif()

    message(${BOLD_GREEN} "Downloaded the toolchain." ${NO_COLOR})

    unpack_toolchain(${DOWNLOAD_PATH} ${TOOLCHAIN_NAME})

    message(${BOLD_GREEN} "Installed " ${TOOLCHAIN_NAME} ${TOOLCHIAN_VERSION}
            " in project's directory." ${NO_COLOR})
endfunction(get_toolchain)


macro(set_toolchain_download_urls)
    set(LINUX_URL https://developer.arm.com/-/media/Files/downloads/gnu-rm/9-2020q2/gcc-arm-none-eabi-9-2020-q2-update-x86_64-linux.tar.bz2?revision=05382cca-1721-44e1-ae19-1e7c3dc96118&la=en&hash=D7C9D18FCA2DD9F894FD9F3C3DC9228498FA281A)
    set(LINUX_MD5 2b9eeccc33470f9d3cda26983b9d2dc6)

    set(MAC_URL https://developer.arm.com/-/media/Files/downloads/gnu-rm/9-2020q2/gcc-arm-none-eabi-9-2020-q2-update-mac.tar.bz2?revision=d0d318de-b746-489f-98b0-9d89648ce910&la=en&hash=9E035CEF6261AA1387D3DCC8B86FA1A20E92B9AB)
    set(MAC_MD5 75a171beac35453fd2f0f48b3cb239c3)

    set(WIN_URL https://developer.arm.com/-/media/Files/downloads/gnu-rm/9-2020q2/gcc-arm-none-eabi-9-2020-q2-update-win32.zip?revision=95631fd0-0c29-41f4-8d0c-3702650bdd74&la=en&hash=D2C7F7C52183A8818AE6179AB87AA7CF6B1AE275)
    set(WIN_MD5 184b3397414485f224e7ba950989aab6)
endmacro(set_toolchain_download_urls)


function(unpack_toolchain TOOLCHAIN_ARCHIVE_PATH TOOLCHAIN_NAME)
    # Uncompress the toolchain
    if(${CMAKE_HOST_SYSTEM_NAME} STREQUAL Windows)
        execute_process(
            COMMAND ${CMAKE_COMMAND} -E make_directory ${TOOLCHAIN_NAME}
            WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
        )
        execute_process(
            COMMAND ${CMAKE_COMMAND} -E tar xvf ${DOWNLOAD_PATH}
            WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}/${TOOLCHAIN_NAME}
        )
    else()
        execute_process(COMMAND ${CMAKE_COMMAND} -E tar xzvf ${DOWNLOAD_PATH}
        WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR})
        # Glob the name of the unpacked toolchain and remove version number from it
        file(GLOB TOOLCHAIN_DIR_NAME ${TOOLCHAIN_NAME}*)
        file(RENAME ${TOOLCHAIN_DIR_NAME} ${TOOLCHAIN_NAME})
    endif()
    # Remove the compressed file
    file(REMOVE ${DOWNLOAD_PATH})
endfunction(unpack_toolchain)
