cmake_minimum_required(VERSION 3.16)

function(setup_libopencm3 LIB_ROOT)
    message(${BOLD_YELLOW} "Building libopencm3 - main hardware abstraction "
            "library." ${NO_COLOR})

    if(NOT CMAKE_BUILD_TYPE)
        set(CMAKE_BUILD_TYPE Release)
    endif()
    if(${CMAKE_BUILD_TYPE} STREQUAL Debug)
        set(LIB_FLAGS "-DDEBUG -g")
    elseif(${CMAKE_BUILD_TYPE} STREQUAL Release)
        set(LIB_FLAGS "-O3")
    endif()

    # Makefile.include is modified version of libopencm3's one
    # It provides capability for handling debug and release builds at once
    execute_process(
        COMMAND ${CMAKE_COMMAND} -E copy
            Makefile.include
            ${LIB_ROOT}/lib
        WORKING_DIRECTORY ${CMAKE_MAIN_PROJECT_DIR}/lib
    )
    execute_process(
        COMMAND ${CMAKE_COMMAND} -E make_directory ${BUILD_DIR}
        WORKING_DIRECTORY ${LIB_ROOT}/lib
    )

    get_filename_component(TOOLCHAIN_ROOT ${CMAKE_C_COMPILER} DIRECTORY)
    execute_process(
        WORKING_DIRECTORY ${LIB_ROOT}
        COMMAND ${CMAKE_MAKE_PROGRAM}
            PREFIX=${TOOLCHAIN_ROOT}/arm-none-eabi-
            SRCLIBDIR=${LIB_ROOT}/lib/${BUILD_DIR}
            TARGETS=${TARGET_DEFINITION}
            CFLAGS=${LIB_FLAGS}
        RESULT_VARIABLE LIBOPENCM3_FAILED
        ERROR_VARIABLE LIBOPENCM3_ERR
        OUTPUT_QUIET
    )

    if(LIBOPENCM3_FAILED)
        message(FATAL_ERROR
            ${BOLD_RED} "Failed to compile libopencm3.\n" ${NO_COLOR}
            ${LIBOPENCM3_ERR}}
        )
    endif()

    execute_process(
        COMMAND git update-index --assume-unchanged ${LIB_ROOT}
    )

    message(${BOLD_GREEN} "Libopencm3 setup done." ${NO_COLOR})
endfunction(setup_libopencm3)

# Project wide global configuration
function(setup_firmware_compiler_options)
    # MCU architecture specific flags
    # Set small instruction size (thumb mode), MCU type and hardware floats calc
    set(ARCH_FLAGS -mthumb -mcpu=${TARGET_MCPU} -mhard-float)

    # Project wide compiler options
    add_compile_options(
        -fno-common # Prevent globals from being put in common ram
        -ffunction-sections # Generate separate ELF section for each function
        -fdata-sections # Enable elf section per variable
        ${ARCH_FLAGS} # Architecture specific flags
        -MD # Create dependencies file
    )
    # C++ compiler exclusive flags
    string(JOIN " " CMAKE_CXX_FLAGS
        "-fno-rtti"
        "-fno-exceptions"
        "-fno-sized-deallocation"
    )
    # Project wide linker options
    add_link_options(
        -T${LD_SCRIPT} ${ARCH_FLAGS} # Specify linker path
        --static # Use only static libraries
        -nostartfiles # Don't use standard library for startup
        -specs=nosys.specs # Use standard library without POSIX OS functions
        -Wl,-Map=${TARGET}.map # Generate a memory map
        -Wl,--cref # Output a cross-reference table to the memory map
        -Wl,--gc-sections # Delete unused code
        -Wl,--print-memory-usage # Print RAM and ROM usage
        # Link: standard library, gcc provided code, stubs for POSIX OS functions
        -Wl,--start-group -lc -lgcc -lnosys -Wl,--end-group
    )
endfunction(setup_firmware_compiler_options)

macro(validate_selected_mcu)
    set(CORTEX_M0 STM32F0 STM32L0 STM32G0)
    set(CORTEX_M3 STM32F1 STM32F2 STM32L1)
    set(CORTEX_M4 STM32F3 STM32F4 STM32L4)
    set(CORTEX_M7 STM32H7 STM32F7)
    set(MCU_ARCHS CORTEX_M0 CORTEX_M3 CORTEX_M4 CORTEX_M7)
    foreach(MCU_ARCH IN LISTS MCU_ARCHS)
        list(APPEND SUPPORTED_MCUS ${${MCU_ARCH}})
    endforeach()

    set(MCU "" CACHE STRING "Target MCU of the project")
    if(NOT MCU IN_LIST SUPPORTED_MCUS)
        message(FATAL_ERROR "Specified MCU is not supported.\n"
            "To properly run this project use cmake -D MCU=<mcu>.\n"
            "Currently these ones are supported: ${SUPPORTED_MCUS}")
    endif()

    foreach(MCU_ARCH IN LISTS MCU_ARCHS)
        if(MCU IN_LIST ${MCU_ARCH})
            set(TARGET_MCPU "" CACHE STRING "Target MCU architecture")
            string(TOLOWER ${MCU_ARCH} TARGET_MCPU)
            string(REPLACE "_" "-" TARGET_MCPU ${TARGET_MCPU})
            break()
        endif()
    endforeach()
endmacro(validate_selected_mcu)

# Set target processor and desired library
macro(set_target_definitions)
    if(MCU MATCHES "STM32.+")
        string(TOLOWER ${MCU} MCU_LOWER_CASE)
        set(TARGET_PROCESSOR ${MCU})
        set(TARGET_LIBRARY opencm3_${MCU_LOWER_CASE})
        string(REPLACE "32" "32/" TARGET_DEFINITION ${MCU_LOWER_CASE})
    endif()
endmacro(set_target_definitions)
