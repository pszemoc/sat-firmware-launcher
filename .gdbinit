file build/src/firmware.elf
target remote | openocd -f "board/st_nucleo_l4.cfg" -c "gdb_port pipe; log_output build/openocd.log"

define restart
	monitor reset halt
end

define reload
	monitor reset halt
	monitor stm32l4x mass_erase 0
	monitor program build/src/firmware.elf verify
	monitor reset halt
end

reload
break main
continue
