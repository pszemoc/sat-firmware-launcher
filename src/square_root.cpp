#include "calculator/square_root.h"
#include <cmath>

using namespace calc;

double Square_root::compute(double x) { return std::sqrt(x); }
