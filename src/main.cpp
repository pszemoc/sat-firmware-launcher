#include <cstdint>
#include <etl/vector.h>
#include <libopencm3/stm32/rcc.h>

#include "satos/kernel.h"

#include "blink/blink.h"

[[noreturn]] int main() {
    constexpr std::size_t bar_size{10};
    etl::vector<int, bar_size> bar;
    int i{};
    for (auto&& b : bar) {
        b = i++;
    }

    rcc_periph_clock_enable(RCC_GPIOA);
    Blink blink{};
    satos::kernel::initialize();
    satos::kernel::start();
    while (true) {}
}
