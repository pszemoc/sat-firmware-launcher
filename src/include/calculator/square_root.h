#ifndef SAT_FIRMWARE_LAUNCHER_SQUARE_ROOT_H
#define SAT_FIRMWARE_LAUNCHER_SQUARE_ROOT_H

namespace calc {
class Square_root {
public:
    virtual double compute(double x);
    virtual ~Square_root() = default;
};
} // namespace calc

#endif // SAT_FIRMWARE_LAUNCHER_SQUARE_ROOT_H
